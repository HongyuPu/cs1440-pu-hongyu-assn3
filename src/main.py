import sys
from Report import Report

# Convert `area_titles.csv` into a dictionary
#############################################

areaDictionary = dict()  # declares dict
areaFile = open(sys.argv[1] + '\\area_titles.csv')  # opens file
word = []
for line in areaFile:
    word = line.split('","')
    for i in range(len(word)):
        word[i] = word[i].replace('"', '')  # strips strings
        word[i] = word[i].replace('\n', '')
    if not word[0].endswith("000") and "U" not in word[0] and "C" not in word[0]:  # makes dictionary
        areaDictionary[word[0]] = word[1]  # appends to dict
areaFile.close()

# Collect stats from `2017.annual.singlefile.csv`
#################################################

file = open(sys.argv[1] + "\\2017.annual.singlefile.csv")  # opens file
data = []
allData = []  # declares arrays
softData = []  # software and all industries
for line in file:
    data = line.split(",")
    for i in range(len(data)):
        data[i] = data[i].replace('"', '')  # strips
        data[i] = data[i].replace('\n', '')
    if data[0] in areaDictionary and data[1] == '0' and data[2] == '10':  # appends to all
        allData.append(data)
    elif data[0] in areaDictionary and data[1] == '5' and data[2] == '5112':  # appends to software
        softData.append(data)
file.close()

# Create the report object
##########################
rpt = Report()


def numAreas(array):
    return len(array)


def grossWage(array):
    sum = 0
    for j in range(len(array)):
        sum += int(array[j][10])
    return sum


def maxWage(array):
    statement = ['', 0]
    for k in range(len(array)):
        if int(array[k][10]) > int(statement[1]):
            statement[0] = array[k][0]
            statement[1] = array[k][10]
    statement[0] = areaDictionary[statement[0]]
    statement[1] = int(statement[1])
    return statement


def totalEstab(array):
    sum = 0
    for j in range(len(array)):
        sum += int(array[j][8])
    return sum


def maxEstab(array):
    statement = ['', 0]
    for k in range(len(array)):
        if int(array[k][8]) > int(statement[1]):
            statement[0] = array[k][0]
            statement[1] = array[k][8]
    statement[0] = areaDictionary[statement[0]]
    statement[1] = int(statement[1])
    return statement


def totalEmploy(array):
    sum = 0
    for j in range(len(array)):
        sum += int(array[j][9])
    return sum


def maxEmploy(array):
    statement = ['', 0]
    for k in range(len(array)):
        if int(array[k][9]) > int(statement[1]):
            statement[0] = array[k][0]
            statement[1] = array[k][9]
    statement[0] = areaDictionary[statement[0]]
    statement[1] = int(statement[1])
    return statement

# Fill in the report for all industries
#######################################
rpt.all.num_areas           = numAreas(allData)

rpt.all.gross_annual_wages  = grossWage(allData)
rpt.all.max_annual_wage     = maxWage(allData)

rpt.all.total_estab         = totalEstab(allData)
rpt.all.max_estab           = maxEstab(allData)

rpt.all.total_empl          = totalEmploy(allData)
rpt.all.max_empl            = maxEmploy(allData)


# Fill in the report for the software publishing industry
#########################################################
rpt.soft.num_areas          = numAreas(softData)

rpt.soft.gross_annual_wages = grossWage(softData)
rpt.soft.max_annual_wage    = maxWage(softData)

rpt.soft.total_estab        = totalEstab(softData)
rpt.soft.max_estab          = maxEstab(softData)

rpt.soft.total_empl         = totalEmploy(softData)
rpt.soft.max_empl           = maxEmploy(softData)


# Print the completed report
############################
print(rpt)
